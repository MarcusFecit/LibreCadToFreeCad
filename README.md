# LibreCadToFreeCad

- <a href="#French" title="Français">Français</a>
- <a href="#English" title="English">English</a>
- <a href="#Italiano" title="Italiano">Italiano</a>

<a name="French"></a>
# Français
Ce script python est un outil de conversion de calques de DXFs LibreCAD vers des croquis (sketches) FreeCAD. Il utilise la librairie ezdxf et permet pour l'instant de convertir les éléments DXF suivants :

- Lignes
- Cercles
- Arcs de Cercles
- Ellipses
- Arcs d'Ellipses
- LW Polylines (décomposées en lignes ou arcs)

À la différence de l'outil d'importation inclus dans FreeCAD, le processus crée un croquis/esquisse (sketch) par calque (layer) présent dans le fichier DXF et tente de générer quelques contraintes de base entre les éléments pour obtenir directement des formes fermées. Parmi lesquelles :

- Longueurs des lignes
- Horizontalité/Verticalité des lignes
- Coincidence entre les points terminaux des lignes, arcs de cercles, et arcs d'ellipses
- Tangentialité entre lignes, arcs de cercles, et arcs d'ellipses
- Rayons des cercles et arcs de cercles
- Distance fixe des centres des cercles et arcs à l'origine

Il est possible de spécifier et ne traiter qu'un seul calque.

En outre, si le nom d'un calque de votre DXF commence par 'XY_', 'XZ_', ou 'YZ_' le script placera directement le sketch dans le plan concerné.
Le script renomme aussi les sketches issus de calques commençant par un chiffre qui ont l'air de poser problème à FreeCAD en mode console.

**Utilisation**

Le programme est un outil en ligne de commande qui s'exécute donc dans un terminal. On le lancera comme ceci :
<pre>
LibreCadToFreeCad.py -i input_file [-o output_file] [-l layer_to_process] [-f fixed_number_of_digits] [-m minutes_of_angle] [-c l,r,o,c,t,h,v,a,0] [-g] [-v] [-d] [-h]
</pre>
Détail des options :
<pre>
       -i input_file              # Le nom du fichier DXF à traiter. Obligatoire !
       -o output_file             # Le nom du fichier de sortie. Par défaut c'est le terminal (stdout)
       -l layer_to_process        # Le calque à traiter. Si non spécifié, tous les calques seront traités
       -f fixed_number_of_digits  # Nombre de décimales à utiliser pour les tests de coincidence : de 0 à 7
       -m minutes_of_angle        # Nombre de minutes de degrés à utiliser pour les tests de tangentialité
       -c l,r,o,c,t,h,v,a         # Sélecteurs de contraintes. Voir description ci-dessous
       -g                         # Ajoute la géométrie interne des ellipses dans FreeCAD.
       -v                         # Traiter seulement les calques visibles
       -d                         # Affiche les informations de débuggage
       -h                         # Affiche l'aide
</pre>
Sélecteurs de contraintes utilisés avec l'option '-c' :
<pre>
        l                         : Longueurs des lignes
        r                         : Longueurs des rayons
        o                         : Distance fixe de l'origine aux centres des cercles et arcs
        c                         : Coincidence entre fins d'entités
        t                         : Tangentialité entre fins d'entités. Implique la coincidence..
        h                         : Horizontalité des lignes
        v                         : Verticalité des lignes
        a                         : Toutes les contraintes supportées. Peut produire des sketches sur-contraints
        0                         : Désactive toutes les contraintes
</pre>
Celles-ci sont activées par défaut : c,t,h,v

Les tests de raccordement sont effectués avec une marge d'erreur correspondant à la tolérance indiquée par l'option '-f'. Ainsi '-f 2' décidera de raccorder des points dont la distance en X ou en Y sera inférieure à 0.01 mm, soit un centième de mm. Par défaut le programme utilise trois décimales (-f 3), soit une tolérance d'un millième de millimètre. Si vos contours ne sont pas complètement raccordés, diminuez la valeur.

Les tests de tangentialité sont effectués avec une tolérance par défaut d'une minute de degré (soit un soixantième). Si les tangentes ne sont pas correctement détectées, augmentez le nombre de minutes d'angle.

Par défaut la sortie s'effectue directement dans le terminal, ou vers un fichier de sortie avec l'option '-o'. Les lignes générées peuvent ensuite être copiées dans la console Python de FreeCAD. Pour transmettre directement le résultat du traitement dans FreeCAD (par exemple pour du traitement par lots), ajouter une "pipe" vers FreeCAD à la ligne de commande. Exemple :
<pre>
LibreCadToFreeCad.py -i mon_beau_fichier.dxf | freecad -c
</pre>
De cette façon, vous créez ainsi un fichier mon_beau_fichier.fcstd contenant tous les sketches correspondant aux calques du fichier DXF d'origine. Bien entendu, il ne faudra pas utiliser l'option '-o'..

Attention, si le fichier .fcstd existe déjà, il sera écrasé

**Astuces**

Pour l'instant les blocs ne sont pas traités. Si dans votre DXF vous avez inséré des blocs et que vous voulez les avoir dans votre sketch FreeCAD, il faut d'abord les "exploser" en utilisant le menu Bloc->Explode de LibreCAD.

***
***
<a name="English"></a>
# English

This python script is a tool to convert LibreCAD DXFs to FreeCAD sketches. It uses the ezdxf library and as of today, it can process the following elements:
- Lines
- Circles
- Circles Arcs
- Ellipses
- Ellipses Arcs
- LW Polylines (exploded to lines or arcs)

Unlike FreeCAD's embedded importing tool, the process creates a sketch for each layer present in the DXF and attempts to create basic constraints between the entities in order to directly obtain closed shapes, such as:

- Lines lengths
- Lines Horizontality/Verticality
- Coincidence between end points of lines, circles arcs, and ellipses arcs
- Tangentiality of lines, circles arcs, and ellipses arcs
- Circles and Circles Arcs radiuses
- Circles and Arcs centers fixed distance from origin

It's possible to specify and process a single layer.

Moreover, if one of your DXF's layer name begins with 'XY_', 'XZ_', or 'YZ_' the script will directly place the sketch in the appropriate plane. The script will also rename the sketches from layers whose name begins with a digit.

**Usage**
<pre>
LibreCadToFreeCad.py -i input_file [-o output_file] [-l layer_to_process] [-f fixed_number_of_digits] [-m minutes_of_angle] [-c l,r,o,c,t,h,v,a,0] [-g] [-v] [-d] [-h]
</pre>
Options details:
<pre>
       -i input_file              # The DXF file to process. Mandatory
       -o input_file              # The output file name. Default is to write in the terminal (stdout)
       -l layer_to_process        # Single layer to process. If not specified, all layers are processed
       -f fixed_number_of_digits  # Number of decimals to use for coincidence tests
       -m minutes_of_angle        # Number of minutes of degrees to use for Tangentiality tests
       -c l,r,o,c,t,h,v,a         # Constraints selection. See description below
       -g                         # Adds ellipses internal geometry in FreeCAD.
       -v                         # Process only visible layers
       -d                         # Shows debugging output
       -h                         # Shows help
</pre>
Constraints selectors, for '-c' option:
<pre>
        l                         : Lines lengths
        r                         : Radius lengths
        o                         : Circles and Arcs centers fixed distance from origin
        c                         : Entities Ends Coincidence
        t                         : Entities Ends Tangentiality. Implies Coincidence..
        h                         : Lines Horizontality
        v                         : Lines Verticality
        a                         : All supported constraints. This can result in over-constraint sketches
        0                         : Disable all constraints
</pre>
These are active by default : c,t,h,v

The coincidence tests are performed with the precision tolerance given by the '-f' option. Thus, with '-f 2', points whose distance in X and Y is less than 0.01 mm will be connected. Default is (-f 3), one thousand of a millimeter. If you sketches are not completely closed, try to decrease the value.

Tangentiality tests are by default performed with an angle tolerance of one minute of degree. If the tangents are not all well detected, increase the number of minutes.

The output will occur by default in the terminal or in a file with the '-o' option. You can then copy/paste the generated lines in the FreeCAD's Python Console. For a direct transmission to FreeCAD (e.g. for batch processing) you can directly pipe the result to FreeCAD. Example :
<pre>
LibreCadToFreeCad.py -i my_beautiful_file.dxf | freecad -c
</pre>

Doing so, you'll create a my_beautiful_file.fcstd FreeCAD file which contains all the sketches from the original DXF file layers. Don't forget not to use the '-o' option..

Caution, if the .fcstd already exists, it will be overwritten

**Hints**

As of today, blocks are not processed. If you have inserted blocks in your DXF, you can still get them in your FreeCAD sketch by "exploding" them first with the Block->Explode menu from LibreCAD.

***
***
<a name="Italiano"></a>
# Italiano
Questo script python è uno strumento per convertire livelli (layers) di DXFs LibreCAD in schizzi (sketches) FreeCAD. Si serve della libreria ezdxf per estrarre le entità. Adesso puo convertire :

- Linee
- Cerchi
- Archi di Cerchi
- Ellissi
- Archi di Ellissi
- LW Polilinee (si verranno suddivise in linee o archi)

A differenza dello strumento di importazione incluso in FreeCAD, il processo crea uno schizzo per ogni livello che si trova nel file DXF e cerca di generare i vincoli di base tra gli elementi per ottenire direttamente una forma chiusa. Vincoli rilevati :

- Lunghezze delle linee
- Orizzontalità/Verticalità delle linee
- Coincidenza tra i punti terminali delle linee, archi de cerchi, e archi di ellissi
- Tangenzialità tra linee, archi de cerchi, e archi di ellissi
- Raggi dei cerchi e archi di cerchi
- Distanza fissa dei centri di cerchi e archi all'origine

E possibile specificare e convertire un solo livello.

Di più, se il nome di un livello del DXF inizia con 'XY_', 'XZ_', ou 'YZ_' lo script posizionerà direttamente lo schizzo nel buon piano.
Lo script rinomina anche i schizzi che vengono di livelli il cui nome inizia con un numero che sembrano essere un problema in FreeCAD.

**Uso**

Il programma è uno strumento da riga di comando che si avvia in un terminale in questo modo:
<pre>
LibreCadToFreeCad.py -i input_file [-o output_file] [-l layer_to_process] [-f fixed_number_of_digits] [-m minutes_of_angle] [-c l,r,o,c,t,h,v,a,0] [-g] [-v] [-d] [-h]
</pre>
Dettagli delle opzioni :
<pre>
       -i input_file              # Nome del file DXF a convertire. Obbligatorio !
       -o output_file             # Nome del file di output. Di default è il terminale (stdout)
       -l layer_to_process        # Il livello a convertire. Se non specificato, tutti i livelli si verranno trattati
       -f fixed_number_of_digits  # Numero di decimali da utilizzare per i test di coincidenza: da 0 a 7
       -m minutes_of_angle        # Numero di minuti di gradi da utilizzare per il test di tangenzialità
       -c l,r,o,c,t,h,v,a         # Selettori di vincoli. Vedi la descrizione qui sotto
       -g                         # Aggiunge la geometria interna delle ellissi in FreeCAD.
       -v                         # Trattare solo i livelli visibili
       -d                         # Mostra informazioni di debug
       -h                         # Visualizza l'aiuto
</pre>
Selettori di vincoli, da usare con opzion '-c' :
<pre>
        l                         : Lunghezze delle linee
        r                         : Raggi dei cerchi e archi di cerchi
        o                         : Distanza fissa dei centri di cerchi e archi all'origine
        c                         : Coincidenza tra i punti terminali degli elementi
        t                         : Tangenzialità tra linee, archi de cerchi, e archi di ellissi. Coinvolge la coincidenza..
        h                         : Orizzontalità delle linee
        v                         : Verticalità delle linee
        a                         : Tutti i vincoli. Puo produrre vincoli ridondanti..
        0                         : Disabilita tutti i vincoli
</pre>
Questi vincoli sono attivi di default: c,t,h,v

I test di connessione vengono eseguiti con un margine di errore corrispondente alla tolleranza indicata dall'opzione '-f'. Quindi '-f 2' deciderà di collegare punti la cui distanza in X o Y sarà inferiore a 0.01 mm, un centesimo di millimetro. Di default il programma utilizza tre decimali (-f 3), corrispondente a una tolleranza di un millesimo di millimetro. Se i contorni non sono completamente collegati, diminuire il valore.

I test di tangenzialità vengono eseguiti con una tolleranza predefinita di un minuto di grado (un sessantesimo). Se le tangenti non vengono rilevate correttamente, aumentare il numero di minuti d'angolo.

Di default, l'output viene eseguito direttamente nel terminale o in un file di output con l'opzione '-o'. Le linee generate possono quindi essere copiate sulla console Python de FreeCAD. Per trasmettere direttamente il risultato del trattamento in FreeCAD (ad esempio per l'elaborazione in batch), aggiungi una "pipa" alla riga di comando. Esempio :
<pre>
LibreCadToFreeCad.py -i il_mio_bello_file.dxf | freecad -c
</pre>
In questo modo, crei un file il_mio_bello_file.fcstd contenente tutti gli schizzi corrispondenti ai livelli del file originale. Ovviamente, non dovrai usare l'opzione '-o'..

Attenzione, se il file .fcstd esiste già, verrà sovrascritto

**Consigli**

Ad oggi, i blocchi non vengono elaborati. Se hai inserito dei blocchi nel tuo DXF, puoi ancora ottenerli nello schizzo di FreeCAD "esplodendoli" per primi con il menu Blocco-> Esplodi da LibreCAD.

***
***

