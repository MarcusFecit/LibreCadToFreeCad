############################################################################ 
# LibreCadToFreeCad
#
# Author: Marc BERLIOUX <github@berlioux.com>
# Copyright: 2018 Marc BERLIOUX
# Licence: GPL3
# Home page: https://framagit.org/MarcusFecit/LibreCadToFreeCad
#
############################################################################ 

Installation (fr) :

Pré-Requis:
Comme l'extraction des entités du DXF s'effectue au moyen de la librairie
'ezdxf', elle doit être installée. Voir la section 'Installation' ici : 

https://github.com/mozman/ezdxf


Copiez le fichier LibreCadToFreeCad.py où bon vous semble, par exemple dans 
un dossier qui figure dans votre $PATH de manière à ce qu'il soit exécutable.

Si le fichier n'est pas considéré comme exécutable essayez :
chmod +x LibreCadToFreeCad.py

Si vous ne l'avez pas copié dans un des dossier de votre $PATH, lancez le 
comme ceci :
./LibreCadToFreeCad.py

############################################################################ 

Installation (en) :

Prerequisite:
As all the DXF's entities extraction is done with the 'ezdxf' python library,
it has to be installed first. See 'Installation' section here :

https://github.com/mozman/ezdxf


Copy the LibreCadToFreeCad wherever you like. A good place is in one of your 
$PATH folders so it can be run easily

If the file is not considered as executable, try :
chmod +x LibreCadToFreeCad.py

If you didn't put it in one of your $PATH folders, simply run it with:
./LibreCadToFreeCad.py

############################################################################ 
